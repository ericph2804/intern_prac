import * as React from "react";
import "../Css/HomePage_5.css";
import { Button, Typography, Box, Paper, Card, CardMedia, CardContent, ButtonGroup } from "@mui/material";
import Carousel from "react-material-ui-carousel";
import KeyboardArrowLeftRoundedIcon from '@mui/icons-material/KeyboardArrowLeftRounded';
import KeyboardArrowRightRoundedIcon from '@mui/icons-material/KeyboardArrowRightRounded';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import Ava_1 from "../Img/ava-1.jpg";
import Ava_2 from "../Img/ava-2.jpg";
import Ava_3 from "../Img/ava-3.jpg";

const btns = [
    <Button variant="contained" sx={{borderRadius:0, backgroundColor:"#fff"}}>
        <KeyboardArrowLeftRoundedIcon sx={{color:"#000"}}/>
    </Button>,
    <Button variant="contained" sx={{borderRadius:0, backgroundColor:"#fff"}}>
        <KeyboardArrowRightRoundedIcon sx={{color:"#000"}}/>
    </Button>,
];

export default function HomePage_5() {
    return(
        <div className="body-5">
            <div className="container-5">
                <div className="header-5">
                    <div className="remove-icon"></div>
                    <Typography variant="h4" className="intro-header intro" sx={{fontWeight:"500"}}>
                        Meet Our Partners
                    </Typography>
                </div>
                <div className="content-5">
                    <Card sx={{borderRadius:0}}>
                        <CardMedia
                            component="img"
                            height="320"
                            image={Ava_1}
                            alt="ava1"
                        />
                        <CardContent>
                            <Typography variant="h6">
                                Nguyễn Mạnh Dzung
                            </Typography>
                            <Typography variant="body2">
                                Senior partner
                            </Typography>
                            <Typography variant="body2">
                                Saigon Office
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{borderRadius:0}}>
                        <CardMedia
                            component="img"
                            height="320"
                            image={Ava_3}
                            alt="ava3"
                        />
                        <CardContent>
                            <Typography variant="h6">
                                Nguyen Ngoc Minh
                            </Typography>
                            <Typography variant="body2">
                                Partner
                            </Typography>
                            <Typography variant="body2">
                                Hanoi Office
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{borderRadius:0}}>
                        <CardMedia
                            component="img"
                            height="320"
                            image={Ava_2}
                            alt="ava2"
                        />
                        <CardContent>
                            <Typography variant="h6">
                                Vu Phuong Trang
                            </Typography>
                            <Typography variant="body2">
                                Partner
                            </Typography>
                            <Typography variant="body2">
                                Saigon Office
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
                <div className="btn-content">
                    <ButtonGroup className="btn-content-group">
                        {btns}
                    </ButtonGroup>
                </div>
                <Button variant="outlined" sx={{borderRadius:"0px"}}>
                    View all people <ArrowRightAltIcon fontSize="large" className="arrowIcon"/>
                </Button>
            </div>
        </div>
    );
}