import React, { useState } from "react";
// import { Carousel } from "react-responsive-carousel";
import { Carousel, Image } from "antd";
import Banner_1 from "../Img/banner-1.jpg";

const contentStyle = {
    height: '160px',
    color: '#fff',
    textAlign: 'center',
    background: '#000',
};

export default function CaroselUI() {
    const [visible, setVisible] = useState(false);
    return (
        <div className="CaroselUI">
            <Carousel autoPlay>
                <div>
                    <Image
                        style={contentStyle}
                        preview={{ setVisible: false }}
                        width= {600}
                        src={Banner_1}
                    />
                </div>

                <div>
                    <Image
                        style={contentStyle}
                        preview={{ setVisible: false }}
                        width= {1100}
                        src={Banner_1}
                    />
                </div>

                <div>
                    <Image
                        style={contentStyle}
                        preview={{ setVisible: false }}
                        width= {1100}
                        src={Banner_1}
                    />
                </div>
            </Carousel>
        </div>
    );
}
