import * as React from "react";
import "../Css/HomePage_6.css";
import { Button, Typography, Box, Paper, Card, CardMedia, CardContent, ButtonGroup } from "@mui/material";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import Bg_3 from "../Img/bg-3.jpg";
import city_1 from "../Img/city-1.jpg";
import city_2 from "../Img/city-2.jpg";
import city_3 from "../Img/city-3.jpg";

export default function HomePage_6() {
    return(
        <div className="body-6">
            <div className="container-6">
                <div>
                    <img src={Bg_3} className="bgimg-6" />
                </div>
                <div className="header-6">
                    <Typography variant="h6" className="intro-header">
                        Resources<br/>
                    </Typography> 
                    <div className="remove-icon"></div>
                    <Typography variant="h4" className="intro-header intro" sx={{fontWeight:"500"}}>
                        News & Announcements
                    </Typography>
                </div>

                <div className="content-6">
                    <Card sx={{borderRadius:0}}>
                            <CardMedia
                                component="img"
                                height="310"
                                image={city_1}
                                alt="city1"
                            />
                            <CardContent>
                                <Typography variant="body2">
                                    Case / DealAnnouncement
                                </Typography>
                                <Typography variant="h6">
                                    Amet minim mollit non deserunt 
                                    2 lines
                                </Typography>
                            </CardContent>
                    </Card>
                    <Card sx={{borderRadius:0}}>
                            <CardMedia
                                component="img"
                                height="310"
                                image={city_2}
                                alt="city1"
                            />
                            <CardContent>
                                <Typography variant="body2">
                                    Case / DealAnnouncement
                                </Typography>
                                <Typography variant="h6">
                                    Amet minim mollit non deserunt 
                                    2 lines
                                </Typography>
                            </CardContent>
                    </Card>
                    <Card sx={{borderRadius:0}}>
                            <CardMedia
                                component="img"
                                height="310"
                                image={city_3}
                                alt="city1"
                            />
                            <CardContent>
                                <Typography variant="body2">
                                    Case / DealAnnouncement
                                </Typography>
                                <Typography variant="h6">
                                    Amet minim mollit non deserunt
                                </Typography>
                            </CardContent>
                    </Card>
                </div>
                <Button variant="contained" sx={{borderRadius:"0px"}}>
                    Read the blog <ArrowRightAltIcon fontSize="large" className="arrowIcon"/>
                </Button>
            </div>
        </div>
    );
}