import * as React from "react";
import "../Css/HomePage_1.css";
import Flags from 'country-flag-icons/react/3x2'
import { Button, Typography } from "@mui/material";
import ArrowDropDownRoundedIcon from '@mui/icons-material/ArrowDropDownRounded';
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import Logo from "../Img/logo.png";
import Banner_1 from "../Img/banner-1.jpg";
import CaroselUI from "./CaroselUI";

const buttons = [
    <Button variant="text">
        ABOUT <ArrowDropDownRoundedIcon fontSize="small" />
    </Button>,
    <Button variant="text">
        PEOPLE
    </Button>,
    <Button variant="text">
        PRACTICES <ArrowDropDownRoundedIcon fontSize="small" />
    </Button>,
    <Button variant="text">
        INDUSTRY <ArrowDropDownRoundedIcon fontSize="small" />
    </Button>,
    <Button variant="text">
        RESOURCES <ArrowDropDownRoundedIcon fontSize="small" />
    </Button>,
    <Button variant="text">
        JOIN 
    </Button>,
    <Button variant="text">
        CONTACT US 
    </Button>,
    <Button variant="text">
        <Flags.US title="United States" /> <ArrowDropDownRoundedIcon fontSize="small" />
    </Button>,
    <Button variant="text">
        <SearchRoundedIcon fontSize="medium"/>
    </Button>,
];

export default function HomePage_1() {
    return (
        <div className="body-1">
            <div className="container-1">
                <div id="header">
                    {/* Menu bar */}
                    <div className="logo">
                        <img src={Logo} alt="llclogo" className="llclogo"/>
                    </div>
                    <div className="menubar">                       
                        {buttons}
                    </div>
                </div>
                <div className="banner">
                        <img src={Banner_1} alt="banner" className="banner_img"/>
                        <div className="banner_text">
                            <Typography variant="h2">
                                Expert lawyers. <br/> 
                                Tailored approach 
                            </Typography> <br/> 
                            <Typography variant="body1" gutterBottom>
                                We secure and defend our clients investments in Vietnam by solving their highly <br/> 
                                contentious and complex matters through all Dispute Resolution alternatives and <br/> 
                                helping them navigate beyond the intricacies of Maritime, Insurance and Corporate & <br/> 
                                Commercial laws.
                            </Typography>
                        </div>
                </div>
            </div>
        </div>
    ); 
};