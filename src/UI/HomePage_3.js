import * as React from "react";
import "../Css/HomePage_3.css";
import { Button, Typography, Box, Paper } from "@mui/material";
import GavelIcon from '@mui/icons-material/Gavel';
import SailingIcon from '@mui/icons-material/Sailing';
import PanToolIcon from '@mui/icons-material/PanTool';
import PaidIcon from '@mui/icons-material/Paid';
import AddBoxSharpIcon from '@mui/icons-material/AddBoxSharp';
import Bg_1 from "../Img/bg-1.jpg";
import course_1 from "../Img/course-1.jpg"


export default function HomePage_3() {
    return(
        <div className="body-3">
            <div className="container-3">
                <div className="bg">
                    <img src={Bg_1} alt="bg_img" className="bg-img"/>
                </div>
                <div className="header-3">
                    <Typography variant="h6" className="intro-header">
                        What we are expert at<br/>
                    </Typography> 
                    <div className="remove-icon"></div>
                    <Typography variant="h4" className="intro-header intro" sx={{fontWeight:"500"}}>
                        Legal Practice Areas
                    </Typography>
                </div>
                <div className="area-content">
                    <Box sx={{                     
                        backgroundColor: "#fff",
                    }} className="box-area-content">
                        <GavelIcon fontSize="large" />
                        <Typography variant="h5" sx={{fontWeight:"bold", color:"#ad0031"}}>
                            Dispute resolution
                        </Typography>
                        <Button><AddBoxSharpIcon sx={{fontSize:54,color:"#ad0031"}} /></Button>
                    </Box>

                    <Box sx={{                    
                        backgroundColor: "#fff",
                    }} className="box-area-content">
                        <SailingIcon fontSize="large" />
                        <Typography variant="h5" sx={{fontWeight:"bold", color:"#ad0031"}}>
                            Maritime & Shipping 
                        </Typography>
                        <Button><AddBoxSharpIcon sx={{fontSize:54,color:"#ad0031"}} /></Button>
                    </Box>

                    <Box sx={{                       
                        backgroundColor: "#fff",
                    }}  className="box-area-content">
                        <PanToolIcon fontSize="large" />
                        <Typography variant="h5" sx={{fontWeight:"bold", color:"#ad0031"}}>
                            Corporate & Commercial 
                        </Typography>
                        <Button><AddBoxSharpIcon sx={{fontSize:54,color:"#ad0031"}} /></Button>
                    </Box>

                    <Box sx={{
                        backgroundColor: "#fff",
                    }} className="box-area-content">
                        <PaidIcon fontSize="large"  />
                        <Typography variant="h5" sx={{fontWeight:"bold", color:"#ad0031"}}>
                            Insurance & Re-insurance
                        </Typography>
                        <Button><AddBoxSharpIcon sx={{fontSize:54,color:"#ad0031"}} /></Button>
                    </Box>
                </div>

                <div className="header-32">
                    <div className="remove-icon"></div>
                    <Typography variant="h4" className="intro-header intro" sx={{fontWeight:"500"}}>
                        Key industry sectors
                    </Typography>
                </div>
                <div className="industry-sector">
                    <Box sx={{
                        backgroundColor: "#ad0031",
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Banking & Finance
                        </Typography>                        
                    </Box>

                    <Box sx={{
                        backgroundColor: "#ad0031",
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Energy & Natural Resources
                        </Typography>                        
                    </Box>

                    <Box sx={{
                        backgroundColor: "#ad0031",
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Infrastructure & Construction
                        </Typography>                        
                    </Box>

                    <Box sx={{
                        backgroundColor: "#ad0031",
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Insurance & Reinsurance
                        </Typography>                        
                    </Box>
                    
                    <Box sx={{
                        backgroundColor: "#ad0031",               
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Logistics & Supply Chain
                        </Typography>                        
                    </Box>
                </div>
                <div className="industry-sector-2">
                <Box sx={{
                        backgroundColor: "#ad0031",
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Marine
                        </Typography>                        
                    </Box>

                    <Box sx={{
                        backgroundColor: "#ad0031",
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Real Estate
                        </Typography>                        
                    </Box>

                    <Box sx={{
                        backgroundColor: "#ad0031",
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Trading & Commodites
                        </Typography>                        
                    </Box>
                    
                    <Box sx={{
                        backgroundColor: "#ad0031",               
                    }} className="industry-content">
                        <Typography variant="h6" sx={{color:"#fff"}}>
                            Investment
                        </Typography>                        
                    </Box>
                </div>
            </div>
        </div>
    );
}