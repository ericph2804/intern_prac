import * as React from "react";
import "../Css/HomePage_7.css";
import { Typography, Box, Paper, Card, CardMedia, CardContent, ButtonGroup, Link, Breadcrumbs } from "@mui/material";
import PhoneOutlinedIcon from '@mui/icons-material/PhoneOutlined';
import EditLocationOutlinedIcon from '@mui/icons-material/EditLocationOutlined';
import MailOutlinedIcon from '@mui/icons-material/MailOutlined';

export default function HomePage_7() {
    return(
        <div className="body-7">
            <div className="container-7">
                <div className="header-7">
                    <div className="remove-icon"></div>
                    <Typography variant="h4" className="intro-header intro" sx={{fontWeight:"500"}}>
                        Contact us
                    </Typography>
                </div>

                <div className="hotline-area">
                    <Typography variant="body1">
                        <PhoneOutlinedIcon className="phoneicon" />Hotline:
                    </Typography>
                    <Breadcrumbs separator="|" sx={{color:"#ad0031"}}>
                        <Link underline="none" color="#ad0031">
                            +84 (0) 903 807 376
                        </Link>
                        <Link underline="none" color="#ad0031">
                            +84 (0) 976 597 636
                        </Link>
                    </Breadcrumbs>
                </div>

                <div className="content-7">
                    {/* Hanoi */}
                    <Box sx={{background:"#083248"}}
                    className="box-content">
                        <Typography variant="h6">
                            Hanoi Office
                        </Typography>
                        <div className="detail hanoi">
                            <p>
                                Unit 6, 11th Floor, HAREC Building,
                                4A Lang Ha Str, Ba Dinh Dist,
                                Hanoi, Vietnam
                            </p>
                            <p id="googlemapParam">
                                <EditLocationOutlinedIcon fontSize="small"/> View on GoogleMap
                            </p>
                            <p id="phoneParam">
                                <PhoneOutlinedIcon fontSize="small"/> + (84-24) 3772 6970
                            </p>
                        </div>
                        <input className="sendmsg-btn" type="submit" value="Send message" />
                    </Box>
                    {/* SaiGon */}
                    <Box sx={{background:"#083248"}}
                    className="box-content">
                        <Typography variant="h6">
                            Sai Gon Office
                        </Typography>
                        <div className="detail saigon">
                            <p>
                                2A - 4A Ton Duc Thang Str.,
                                Ben Nghe Ward, D.1,
                                Ho Chi Minh City, Vietnam
                            </p>
                            <p id="googlemapParam">
                                <EditLocationOutlinedIcon fontSize="small"/> View on GoogleMap
                            </p>
                            <p id="phoneParam">
                                <PhoneOutlinedIcon fontSize="small"/> + (84-28) 3822 0076
                            </p>
                        </div>
                        <input className="sendmsg-btn" type="submit" value="Send message" />
                    </Box>
                    {/* Emergency Response */}
                    <Box sx={{background:"#083248"}}
                    className="box-content">
                        <Typography variant="h6">
                            Emergency Response
                        </Typography>
                        <div className="detail emergency shipping">
                            <p>
                                Shipping:
                            </p>
                            <p>
                                Nguyen Manh Dzung - Senior Partner
                            </p>
                            <p id="phoneParam">
                                <PhoneOutlinedIcon fontSize="small"/> + 84 (0) 903 807 376
                            </p>
                            <p id="mailParam">
                                <MailOutlinedIcon fontSize="small"/> dzung.nguyen@dzungsrt.com
                            </p> <br/>
                        </div> <br/>

                        <div className="detail emergency dispute">
                            <p>
                                Dispute Resolution:
                            </p>
                            <p>
                                Nguyen Ngoc Minh - Partner
                            </p>
                            <p id="phoneParam">
                                <PhoneOutlinedIcon fontSize="small"/> + 84 (0) 976 597 636
                            </p>
                            <p id="mailParam">
                                <MailOutlinedIcon fontSize="small"/> minh.nguyen@dzungsrt.com
                            </p> <br/>
                        </div>
                    </Box>
                </div>
            </div>
        </div>
    );
}