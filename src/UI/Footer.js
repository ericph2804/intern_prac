import * as React from "react";
import "../Css/Footer.css";
import { Link, Typography, TextField, Breadcrumbs, Stack } from "@mui/material";
import Logo from "../Img/logo.png";
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

const breadcrumbs = [
    <Link underline="none" color="inherit" href="#">
        Disclaimer
    </Link>,

    <Link underline="none" color="inherit" href="#">
        Careers & Opportunities
    </Link>,

    <Typography variant="body1">
        © 2021 Digital Mekong. All rights reserved.
    </Typography>
];

export default function Footer() {
    return(
        <div className="body-footer">
            <div className="footer">
                <div className="logo">
                    <img src={Logo} alt="llclogo" className="llclogo"/>
                </div>
                <div className="footer-about">
                    <Typography variant="button" fontWeight="bold">
                        About
                    </Typography>
                    <Link variant="caption" underline="none">
                        Our DNA
                    </Link>
                    <Link variant="caption" underline="none">
                        Philosophy
                    </Link>
                    <Link variant="caption" underline="none">
                        Market Recognition
                    </Link>
                    <Link variant="caption" underline="none">
                        Social commitment
                    </Link>
                </div>

                <div className="footer-practices">
                    <Typography variant="button" fontWeight="bold">
                        Practices
                    </Typography>
                    <Link variant="caption" underline="none">
                        Dispute Resolution
                    </Link>
                    <Link variant="caption" underline="none">
                        Shipping
                    </Link>
                    <Link variant="caption" underline="none">
                        Corporate & Commercial
                    </Link>
                    <Link variant="caption" underline="none">
                        Insurance & Reinsurance
                    </Link>
                </div>

                <div className="footer-contact">
                    <Typography variant="button" fontWeight="bold">
                        Practices
                    </Typography>
                    <div className="community">
                        <a><LinkedInIcon fontSize="small" className="socialIcon" /> </a>
                        <a><FacebookRoundedIcon fontSize="small" className="socialIcon" /></a>
                        <a><TwitterIcon fontSize="small" className="socialIcon" /></a>
                        <a><YouTubeIcon fontSize="small" className="socialIcon" /></a>
                    </div>
                    <Link variant="button" underline="none">
                        Subscribe for newsletter
                    </Link>
                    <div className="form-contact">
                        <TextField variant="filled" label="Email Address" sx={{backgroundColor:"#fff"}} className="text-form" />
                        <input className="sub-btn" type="submit" value="Subscribe" />
                    </div>
                </div>
            </div>

            <div className="copyright">
                <Stack spacing={2}>
                    <Breadcrumbs separator="|" sx={{color:"#8f8f8f"}}>
                        {breadcrumbs}
                    </Breadcrumbs>
                </Stack>
            </div>
        </div>
    );
}