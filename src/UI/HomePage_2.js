import * as React from "react";
import "../Css/HomePage_2.css";
import { Card, CardActions, CardHeader, CardContent,
Button, Typography, CardMedia, Box, Link, Grid } from "@mui/material";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import course_1 from "../Img/course-1.jpg";
import course_2 from "../Img/course-2.jpg";
import course_3 from "../Img/course-3.jpg";
import course_4 from "../Img/course-4.jpg";

export default function HomePage_2() {
    return (
        <div className="body-2">
            <Grid container className="container">
                <Grid xs={3} className="grid-content">
                    <Box className="box-content">
                        <Card sx={{ borderRadius: 0}} variant="elevation">
                            <div className="course-content-bg">
                                <CardMedia
                                    component="img"
                                    height="328"
                                    image={course_1}
                                    id="course-img-1"
                                />
                            </div>
                            <CardContent>
                                <Typography variant="h3" fontWeight={600}>
                                        <span className="course-content">
                                            _ <br/>
                                            Our DNA
                                        </span>
                                </Typography>
                            </CardContent>
                            <div className="link-content">
                                <CardActions className="link-content-card">
                                    <Link variant="subtitle1" href="#" underline="none" color="#fff">
                                        Learn more <ArrowRightAltIcon fontSize="large" className="arrowIcon"/>
                                    </Link>
                                </CardActions>
                            </div>
                        </Card>
                    </Box>
                </Grid>              

                <Grid xs={3}>
                    <Box className="box-content">
                        <Card sx={{ borderRadius: 0}} variant="elevation">
                            <div className="course-content-bg">
                                <CardMedia
                                    component="img"
                                    height="328"
                                    image={course_3}
                                    id="course-img-1"
                                />
                            </div>
                            <CardContent>
                                <Typography variant="h3" fontWeight={600}>
                                        <span className="course-content">
                                            _ <br/>
                                            Philosophy
                                        </span>
                                </Typography>
                            </CardContent>
                            <div className="link-content">
                                <CardActions className="link-content-card">
                                    <Link variant="subtitle1" href="#" underline="none" color="#fff">
                                        Learn more <ArrowRightAltIcon fontSize="large" className="arrowIcon"/>
                                    </Link>
                                </CardActions>
                            </div>
                        </Card>
                    </Box>
                </Grid>

                <Grid xs={3}>
                    <Box className="box-content">
                        <Card sx={{ borderRadius: 0}} variant="elevation">
                            <div className="course-content-bg">
                                <CardMedia
                                    component="img"
                                    height="328"
                                    image={course_4}
                                    id="course-img-1"
                                />
                            </div>
                            <CardContent>
                                <Typography variant="h3" fontWeight={600}>
                                        <span className="course-content">
                                            _ <br/>
                                            Market Recogniton
                                        </span>
                                </Typography>
                            </CardContent>
                            <div className="link-content-2">
                                <CardActions className="link-content-card-2">
                                    <Link variant="subtitle1" href="#" underline="none" color="#fff">
                                        Learn more <ArrowRightAltIcon fontSize="large" className="arrowIcon"/>
                                    </Link>
                                </CardActions>
                            </div>
                        </Card>
                    </Box>
                </Grid>

                <Grid xs={3}>
                    <Box className="box-content">
                        <Card sx={{ borderRadius: 0}} variant="elevation">
                            <div className="course-content-bg">
                                <CardMedia
                                    component="img"
                                    height="328"
                                    image={course_2}
                                    id="course-img-1"
                                />
                            </div>
                            <CardContent>
                                <Typography variant="h3" fontWeight={600}>
                                        <span className="course-content">
                                            _ <br/>
                                            Social Commitment
                                        </span>
                                </Typography>
                            </CardContent>
                            <div className="link-content-2">
                                <CardActions className="link-content-card-2">
                                    <Link variant="subtitle1" href="#" underline="none" color="#fff">
                                        Learn more <ArrowRightAltIcon fontSize="large" className="arrowIcon"/>
                                    </Link>
                                </CardActions>
                            </div>
                        </Card>
                    </Box>
                </Grid>
            </Grid>
        </div>
    );
}