import * as React from "react";
import "../Css/HomePage_4.css";
import { Button, Typography, Box, Paper } from "@mui/material";
import Bgimg from "../Img/bg-2.jpg"
import Logo from "../Img/logo-2.png"

export default function HomePage_4() {
    return(
        <div className="body-4">
            <div className="container-4">
                <div className="bg-container">
                    <img src={Bgimg} alt="bgimg" className="bgimg"/>
                </div>
                <div className="frame-content">
                    <div className="header-4">
                        <div className="remove-icon"></div>
                        <Typography variant="h4" className="intro-header intro" sx={{fontWeight:"500"}}>
                            Why Clients Choose Us?
                        </Typography>
                    </div>
                    <div className="param-content">
                        <Typography variant="body1" color="#fff">
                        Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia <br/>
                        consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.
                        </Typography>
                    </div>
                    <div className="logo-content">
                        <img src={Logo} alt="logo1" className="logoimg"/>
                        <img src={Logo} alt="logo1" className="logoimg"/>
                        <img src={Logo} alt="logo1" className="logoimg"/>
                        <img src={Logo} alt="logo1" className="logoimg"/>
                        <img src={Logo} alt="logo1" className="logoimg"/>
                    </div>
                    <div className="data-content">
                        <div className="data-client">
                            <Typography variant="h4" fontWeight="500">
                                1000+
                            </Typography>
                            <Typography variant="body1" fontWeight="500">
                                Client Consultations
                            </Typography>
                        </div>
                        <div className="data-client">
                            <Typography variant="h4" fontWeight="500">
                                1000+
                            </Typography>
                            <Typography variant="body1" fontWeight="500">
                                Client Consultations
                            </Typography>
                        </div>
                        <div className="data-client">
                            <Typography variant="h4" fontWeight="500">
                                1000+
                            </Typography>
                            <Typography variant="body1" fontWeight="500">
                                Client Consultations
                            </Typography>
                        </div>
                        <div className="data-client">
                            <Typography variant="h4" fontWeight="500">
                                1000+
                            </Typography>
                            <Typography variant="body1" fontWeight="500">
                                Client Consultations
                            </Typography>
                        </div>
                    </div>

                    <Button variant="outlined" sx={{borderRadius:"0px"}}>Learn more</Button>
                </div>
            </div>
        </div>
    );
}