import React from "react";
import "./Css/App.css"
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import HomePage_1 from "./UI/HomePage_1";
import HomePage_2 from "./UI/HomePage_2";
import HomePage_3 from "./UI/HomePage_3";
import HomePage_4 from "./UI/HomePage_4";
import HomePage_5 from "./UI/HomePage_5";
import HomePage_6 from "./UI/HomePage_6";
import HomePage_7 from "./UI/HomePage_7";
import Footer from "./UI/Footer";

const App = () => {
    return (
        <Router>
            <Switch>
                <Route path="/">               
                    <HomePage />
                </Route>
            </Switch>
        </Router>

    );
}

export default App;

function HomePage () {
    return <div>
        <div>
            <HomePage_1 />
        </div>
        <div>
            <HomePage_2 />
        </div>
        <div>
            <HomePage_3 />
        </div>
        <div>
            <HomePage_4 />
        </div>
        <div>
            <HomePage_5 />
        </div>
        <div>
            <HomePage_6 />
        </div>
        <div>
            <HomePage_7 />
        </div>
        <div>
            <Footer />
        </div>
    </div>
}